x = 0
while x < 7:
    print("x =", x)
    x += 1

x = 0
while x < 7:
    if x == 2:
        x += 1
        continue # skip current iteration
    if x == 6:
        break # leave loop
    print("x =", x)
    x += 1
