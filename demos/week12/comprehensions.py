x = [1, 2, 3]
a = [val + 1 for val in x] # [2, 3, 4]

# can add a condition
b = [val + 1 for val in x if val != 2] # [2, 4]

y = dict(x=1, y=2, z=3)
a = {key + '_a': val + 1 for key, val in y.items() if val != 3}
# a == {'x_a': 2, 'y_a': 3}
