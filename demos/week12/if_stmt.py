x = 0
# if
if x < 0:
    print("x is less than zero")

# if ... else
if x < 0:
    print("x is less than zero")
else:
    print("x is greater or equal zero")

# if ... elif ... else
if x < 0:
    print("x is less than zero")
elif x > 0:
    print("x is greater than zero")
else:
    print("x is zero")
