#include <cmath>
#include <iostream>

// base class provides the function interface
struct SimpleFunction {
  SimpleFunction() {}
  virtual double operator()(double) const = 0;
};

// trapezoidal rule
double integrate(SimpleFunction const& f, double a, double b,
                 unsigned int N) {
  double dx = (b - a )/N;
  double xi = a;
  double I = 0.5*f(xi);
  for (unsigned int i = 1; i < N; ++i) {
    xi += dx;
    I += f(xi);
  }
  I += 0.5*f(b);
  return I*dx;
}

// derived class (implements a function!)
struct MyFunc1 : SimpleFunction {
  MyFunc1() {}
  double operator()(double x) const { return x*std::sin(x); }
};

// derived class (implements another function!)
struct MyFunc2 : SimpleFunction {
  MyFunc2() {}
  double operator()(double x) const { return x*x*std::sin(x); }
};

int main() {
  MyFunc1 f1;
  MyFunc2 f2;
  std::cout <<"I[x*sin(x)] = " << integrate(f1, 0., 1., 100) << '\n';

  return 0;
}
