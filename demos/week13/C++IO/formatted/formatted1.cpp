#include <iostream>

int main() {

  // integers
  std::cout << "Integers:\n";
  std::cout << "|" <<  1         << "|\n";  // |1|
  std::cout << "|" << -1         << "|\n";  // |-1|
  std::cout << "|" <<  123456789 << "|\n";  // |123456789|
  std::cout << "|" << -123456789 << "|\n";  // |-123456789|

  // floating-point
  std::cout << "Floating-point numbers:\n";
  // trailing zeros not displayed
  std::cout << "|" <<  1.20000       << "|\n"; // |1.2|
  // default precision is 6 digits
  std::cout << "|" <<  1.23456       << "|\n"; // |1.23456|
  std::cout << "|" << -1.23456       << "|\n"; // |-1.23456|
  std::cout << "|" <<  1.234567      << "|\n"; // |1.23457|
  std::cout << "|" <<  123456.7      << "|\n"; // |123457|
  // scientific-notation for exponent >=6
  std::cout << "|" <<  1234567.89    << "|\n"; // |1.23457e+006|
  // leading zeros not counted for precision
  std::cout << "|" <<  0.0001234567  << "|\n"; // |0.000123457|
  // scientific-notation for exponent <=-5
  std::cout << "|" <<  0.00001234567 << "|\n"; // |1.23457e-005|

}
