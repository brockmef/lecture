#include <iostream>
#include <iomanip>

int main() {

  // std::setw() is non-sticky!
  std::cout << "|" << std::setw(5) <<  123 << "|" << 123 << '\n';
  // minus sign is included in field width!
  std::cout << "|" << std::setw(5) << -123    << "|\n";
  // no truncation of data
  std::cout << "|" << std::setw(5) << 1234567 << "|\n";
 
  // std::setfill() is sticky!
  std::cout << std::setfill('_');
  std::cout << std::setw(6) << 123 << std::setw(4) << 12 << '\n';
  std::cout << std::setw(12) << 12356 << '\n';
  std::cout << std::setfill(' '); // because it's sticky, we reset it!

  // alignment
  std::cout << std::showpos; // show positive sign
  std::cout << '|' << std::setw(6) <<  123 << "|\n";  // |  +123|
  std::cout << std::left
            << '|' << std::setw(6) << -123 << "|\n";  // |+123  |
  std::cout << std::right
            << '|' << std::setw(6) <<  123 << "|\n";  // |  +123|
  std::cout << std::internal
            << '|' << std::setw(6) << -123 << "|\n";  // |+  123|

}
