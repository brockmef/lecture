import numpy as np

# create some arrays
a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
b = np.diag([2, 2, 2])

a - b
a*b
b/a
a**b

H = b - np.diag([1, 1], -1) - np.diag([1, 1], +1)

np.sin(a)
np.cos(b)
np.sqrt(a)
np.sum(a)
np.min(a)
