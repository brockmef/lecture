import numpy as np

# some basics
a = np.array([[0, 1, 2], [3, 4, 5]])
print("a =", a)
print("a.ndim =", a.ndim)
print("a.shape =", a.shape)
print("a.size =", a.size)
print("a.dtype =", a.dtype)

# more example of array creation
a = np.array([[8, 7, 6], [5, 4, 3], [2, 1, 0]])

a = np.arange(0, 10, 2, dtype=np.float64)

a = np.linspace(0, 1, 11)

a = np.empty((3, 2, 5))

a = np.zeros((5, 3, 2))

a = np.zeros(32, dtype=bool)

a = np. ones((2, 3, 5))

b = np.zeros_like(a)

a = np.random.random((3, 3))
