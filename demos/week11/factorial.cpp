#include <iostream>

// the general factorial
template<int N>
struct Factorial {
  enum { value = N * Factorial<N-1>::value };
};

// the specialization that stops the recursion
template<>
struct Factorial<1> {
  enum { value = 1 };
};

int main() {
  std::cout << "Factorial< 5>::value = " << Factorial< 5>::value << "\n";
  std::cout << "Factorial<11>::value = " << Factorial<11>::value << "\n";
}
