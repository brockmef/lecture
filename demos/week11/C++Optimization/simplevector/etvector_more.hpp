#include <algorithm> // for std::swap
#include <cassert>

/*
This version contains more operators and tries not figure out a proper
way of dealing with ETs in a general way. However, it fails :-)
It tries to look at the generated expression types in timeet_more.cpp
*/

// This class encapsulates the "+" operation.
struct plus {
  public:
    static inline double apply(double a, double b) {
      return a + b;
    };
};

// This class encapsulates the "-" operation.
struct minus {
  public:
    static inline double apply(double a, double b) {
      return a - b;
    };
};

// This class encapsulates the "*" operation.
struct mult {
  public:
    static inline double apply(double a, double b) {
      return a * b;
    };
};

// This class encapsulates the "/" operation.
struct divide {
  public:
    static inline double apply(double a, double b) {
      return a / b;
    };
};

// eXpression
template<typename Left,typename Op,typename Right>
class X {
  public:
    // ctor
    X(const Left& x, const Right& y) : left_(x), right_(y) { }
    // subscript operator
    double operator[](int i) const {
      return Op::apply(left_[i], right_[i]);
    }

  private:
    const Left& left_;
    const Right& right_;
};

// etvector class
template <typename T>
class etvector {
  public:
    typedef T value_type;
    typedef T& reference;
    typedef unsigned int size_type;
    // ctor
    explicit etvector(size_type s=0) : p_(new value_type[s]), sz_(s) {}
    // copy ctor
    etvector(const etvector& v) : p_(new value_type[v.size()])
                                , sz_(v.size()) {
      for (int i=0; i<size(); ++i) {
        p_[i] = v.p_[i];
      }
    }
    // dtor
    ~etvector() { delete[] p_;}
    // swap
    void swap(etvector& v) {
      std::swap(p_,v.p_);
      std::swap(sz_,v.sz_);
    }
    // copy assignment
    const etvector& operator=(etvector v) {
      swap(v);
      return *this;
    }
    // assignment from eXpression object
    template <typename L,typename Op,typename R>
    const etvector& operator=(const X<L,Op,R>& v) {
      for (int i=0; i<size(); ++i) {
        p_[i] = v[i];
      }
      return *this;
    }
    // size
    size_type size() const { return sz_; }
    // subscript operator
    value_type operator[](size_type i) const { return p_[i]; }
    reference operator[](size_type i) { return p_[i]; }

  private:
    value_type* p_;
    size_type sz_;
};

// binary "+" operator (non-member / free function)
template<typename Left,typename Right>
inline X<Left,plus,Right > operator+(const Left& a,
                                     const Right& b) {
  return X<Left,plus,Right >(a, b);
}
template<typename Left,typename T>
inline X<Left,plus,etvector<T> > operator+(const Left& a,
                                           const etvector<T>& b) {
  return X<Left,plus,etvector<T> >(a, b);
}
template<typename T,typename Right>
inline X<etvector<T>,plus,Right > operator+(const etvector<T>& a,
                                            const Right& b) {
  return X<etvector<T>,plus,Right >(a, b);
}
template<typename T>
inline X<etvector<T>,plus,etvector<T> > operator+(const etvector<T>& a,
                                                  const etvector<T>& b) {
  return X<etvector<T>,plus,etvector<T> >(a, b);
}

// binary "-" operator (non-member / free function)
template<typename Left, typename T>
inline X<Left,minus,etvector<T> > operator-(const Left& a,
                                            const etvector<T>& b) {
  return X<Left,minus,etvector<T> >(a, b);
}

// binary "*" operator (non-member / free function)
template<typename Left,typename T>
inline X<Left,mult,etvector<T> > operator*(const Left& a,
                                           const etvector<T>& b) {
  return X<Left,mult,etvector<T> >(a, b);
}

// binary "/" operator (non-member / free function)
template<typename Left,typename T>
inline X<Left,divide,etvector<T> > operator/(const Left& a,
                                             const etvector<T>& b) {
  return X<Left,divide,etvector<T> >(a, b);
}
