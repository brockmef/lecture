#include "sarray.hpp"
#include <iostream>

int main() {

  // create SArray with five elements
  using elem_t = int;
  SArray<elem_t> a(5);

  // set some values (using index access!)
  for (SArray<elem_t>::size_type i=0; i < a.size(); ++i) {
    a[i] = i;
  }

    // print the array (using pointers)
  std::cout << "a = ";
  for (SArray<elem_t>::value_type* p=&a[0]; p!=&a[0]+a.size(); ++p) {
    std::cout << *p << " ";
  }
  std::cout << '\n';

  // print the array using our iterator
  std::cout << "a = ";
  for (SArray<elem_t>::iterator p=a.begin(); p!=a.end(); ++p) {
    std::cout << *p << " ";
  }
  std::cout << '\n';

  // even more convenient: print array using range-based for
  std::cout << "a = ";
  for (auto const& elem : a) {
    std::cout << elem << " ";
  }
  std::cout << '\n';

  return 0;
}
