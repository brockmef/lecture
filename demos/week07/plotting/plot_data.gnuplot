set title 'Complexity'
set xlabel 'N'
set ylabel 'f(N)'

set log xy
set grid
set key top left

set terminal png
set output 'complexity.png'

plot 'data.dat' u 1:2 w lp ti 'log(N)',   \
     'data.dat' u 1:3 w lp ti 'N',        \
     'data.dat' u 1:4 w lp ti 'N*log(N)', \
     'data.dat' u 1:5 w lp ti 'N**2',     \
     'data.dat' u 1:6 w lp ti 'N**3',     \
     'data.dat' u 1:7 w lp ti '2**N'
