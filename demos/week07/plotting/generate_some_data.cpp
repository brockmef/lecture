#include <cmath>
#include <iostream>

int main() {
  // header
  std::cout << "# N "<< "log(N) " << "N " << "N*log(N) "
                     << "N**2 " << "N**3 " << "2**N \n";
  // print data
  for (int N = 0; N < 1000; ++N ) {
    std::cout << N << " " << std::log(N)
                   << " " << N
                   << " " << N*std::log(N)
                   << " " << N*N
                   << " " << N*N*N
                   << " " << std::pow(2, N)
                   << "\n";
  }

  return 0;
}
