#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

int main() {
  {
    std::vector<int> v(10);
    // std::string s(10);
  }

  {
    std::vector<int> v = {1, 2, 3, 4, 5};
    std::cout << *std::find(v.begin(), v.end(), 4) << std::endl;
    // std::cout << std::find(v.begin(), v.end(), 4) << std::endl;
    // std::cout << std::find(v.begin(), v.end, 4) << std::endl;
    std::cout << *std::find_if(v.begin(), v.end(),
                               [](const int& e){ return e == 4; })
              << std::endl;
    // std::cout << *std::find(v.begin(), v.end(),
    //                         [](const int& e){ return e == 4; })
    //           << std::endl;
  }

  return 0;
}
