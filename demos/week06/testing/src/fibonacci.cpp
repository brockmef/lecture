#include "fibonacci.hpp"

uint64_t fibonacci(uint32_t n) {
  if (n==0)
    return 0;
  
  uint64_t f_i(1);          // f(1)
  uint64_t f_i_minus_1(0);  // f(0)
  uint64_t temp(0);

  for (uint32_t i = 2; i <= n; ++i) {
    temp = f_i + f_i_minus_1;  // f(i) = f(i-1) + f(i-2)
    f_i_minus_1 = f_i;
    f_i = temp;
  }
  
  return f_i;
}
