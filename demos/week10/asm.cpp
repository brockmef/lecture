int main() {
  asm ("movq $60, %rax\n\t" // the exit syscall number on Linux
       "movq $2,  %rdi\n\t" // this program returns 2
       "syscall");
}
