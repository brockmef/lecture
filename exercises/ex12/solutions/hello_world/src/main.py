#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Programming Techniques for Scientific Simulations, ETH Zurich

# Import module 'hello'.
import hello

# Import object 'world' from submodule 'world' of package 'include'.
from include.world import world

if __name__ == '__main__':
    hello.hello()
    world()
