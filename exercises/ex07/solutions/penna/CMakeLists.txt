# Require 3.1 for CMAKE_CXX_STANDARD property
cmake_minimum_required(VERSION 3.1)

project(penna)

set(CMAKE_CXX_STANDARD 11)
set(DIRECTORIES src test)

foreach(directory ${DIRECTORIES})
  add_subdirectory(${directory})
endforeach(directory)

# This needs to be in both the top-level and the test CMake file.
enable_testing()

include_directories(${CMAKE_SOURCE_DIR}/src)

add_executable(penna_sim main.cpp)
target_link_libraries(penna_sim penna)
