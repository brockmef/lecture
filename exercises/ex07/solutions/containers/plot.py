#!/usr/bin/env python
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt


def main():
    parser = ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("output_file", nargs="?")
    args = parser.parse_args()

    data = np.loadtxt(args.input_file)

    fig = plt.figure()
    # fig.gca().set_xscale("log", base=2)
    # fig.gca().set_yscale("log", base=10)
    plt.plot(data[:, 0], data[:, 1], label="Vector")
    plt.plot(data[:, 0], data[:, 2], label="List")
    plt.plot(data[:, 0], data[:, 3], label="Set")
    plt.legend()
    plt.title("Standard container comparison")
    plt.xlabel("Size in elements")
    plt.ylabel("Time in ns / op")

    if args.output_file:
        plt.savefig(args.output_file)
    else:
        plt.show()


if __name__ == "__main__":
    main()
