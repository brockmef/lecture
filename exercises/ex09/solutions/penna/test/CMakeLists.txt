include_directories(${PROJECT_SOURCE_DIR}/src)
link_directories(${PROJECT_BINARY_DIR}/src)

add_executable(genome-test-default genome_test_default_ctor.cpp)
add_executable(genome-test-copy genome_test_copy_ctor.cpp)

target_link_libraries(genome-test-default penna)
target_link_libraries(genome-test-copy penna)

enable_testing()

add_test(NAME Genome-Default-Constructor COMMAND genome-test-default)
add_test(NAME Genome-Copy-Constructor COMMAND genome-test-copy)
